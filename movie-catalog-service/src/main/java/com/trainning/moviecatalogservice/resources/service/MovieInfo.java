package com.trainning.moviecatalogservice.resources.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.trainning.moviecatalogservice.models.CatalogItem;
import com.trainning.moviecatalogservice.models.Movie;
import com.trainning.moviecatalogservice.models.Rating;

@Service
public class MovieInfo {

	@Autowired
	private RestTemplate restTemplate;

	@HystrixCommand(fallbackMethod = "getMovieCatalogfallback",commandProperties = {@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "3000"),
		       @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value="60")})
	public CatalogItem getMovieCatalog(Rating rating) {
		Movie movie = restTemplate.getForObject("http://movie-info-service/movies/" + rating.getMovieId(), Movie.class);
		return new CatalogItem(movie.getName(), movie.getDescription(), rating.getRating());
	}

	public CatalogItem getMovieCatalogfallback(Rating rating) {
		return new CatalogItem("Movie Not Found", "", 0);
	}

}
