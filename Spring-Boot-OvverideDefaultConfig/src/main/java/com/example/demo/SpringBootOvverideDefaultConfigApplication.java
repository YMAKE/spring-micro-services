package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.DispatcherServletAutoConfiguration;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.DispatcherServlet;

@SpringBootApplication
@RestController
public class SpringBootOvverideDefaultConfigApplication {
	
	@Bean
	public DispatcherServlet dispacthServlet() {
		return new DispatcherServlet();
	}
	
	@Bean
	public ServletRegistrationBean registrationBean() {
		ServletRegistrationBean registrationBean=new ServletRegistrationBean(dispacthServlet(), "/api/*");
		registrationBean.setName(DispatcherServletAutoConfiguration.DEFAULT_DISPATCHER_SERVLET_BEAN_NAME);
		return registrationBean;
	}
	
	@RequestMapping("/")
	public String getTheCustomeRequest() {
		return "Good To See You";
	}

	public static void main(String[] args) {
		SpringApplication.run(SpringBootOvverideDefaultConfigApplication.class, args);
	}

}

