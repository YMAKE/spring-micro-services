package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;

@SpringBootApplication
@EnableEurekaClient
@RestController
public class EurekaClient2Application {
	
	@Autowired
	private EurekaClient eurekaClient;
	
	@RequestMapping("/serviceInfo")
	public String getServiceInfo() {
		InstanceInfo  instanceInfo=eurekaClient.getNextServerFromEureka("Eureka-Client1", false);
		return instanceInfo.getHomePageUrl();
	}

	public static void main(String[] args) {
		SpringApplication.run(EurekaClient2Application.class, args);
	}

}

