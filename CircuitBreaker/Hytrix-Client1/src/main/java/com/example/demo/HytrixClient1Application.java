package com.example.demo;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

@SpringBootApplication
@RestController
@EnableHystrix
public class HytrixClient1Application {
	
	@Bean
	public RestTemplate createTemplate() {
		return new RestTemplate();
	}
	
	@Autowired
	public RestTemplate restTemplate;
	
	@RequestMapping("/")
	@HystrixCommand(fallbackMethod="routeTrafficToDefaultMethod",commandProperties={
		@HystrixProperty(name="execution.isolation.thread.timeoutInMilliseconds", value="500")
	})
	public List<String> getColorsInfo(@RequestParam long timeOut) throws InterruptedException{
		Thread.sleep(timeOut);
		return restTemplate.getForObject("http://localhost:9999/colors", List.class);
	}
	
	public List<String> routeTrafficToDefaultMethod(long timeOut){
		return Arrays.asList("Green_Default1","Green_Default2");
	}

	public static void main(String[] args) {
		SpringApplication.run(HytrixClient1Application.class, args);
	}

}

