package com.example.demo;


import java.util.Arrays;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class SimpleMicroServiceApplication {
	
	@RequestMapping("/colors")
	public List<String> getColors() {
		return Arrays.asList("White","Yellow","Black");
	}

	public static void main(String[] args) {
		SpringApplication.run(SimpleMicroServiceApplication.class, args);
	}

}

