package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableZuulProxy
public class ZuulMicroServiceApplication {
	
	@Bean
	public MyZuulFilter createFilterBean() {
		return new MyZuulFilter();
	}

	public static void main(String[] args) {
		SpringApplication.run(ZuulMicroServiceApplication.class, args);
	}

}

