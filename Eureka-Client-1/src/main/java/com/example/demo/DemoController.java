package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class DemoController {
	
	@Autowired
	private RestTemplate restTemplate;
	
	@RequestMapping("/requestFromClient1")
	public String requestFromClient1() {
		return restTemplate.getForObject("http://EUREKA-CLIENT2/serviceInfo", String.class);
	}

}
