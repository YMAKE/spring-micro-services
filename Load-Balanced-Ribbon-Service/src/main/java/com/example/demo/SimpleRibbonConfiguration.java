package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;

import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.AvailabilityFilteringRule;
import com.netflix.loadbalancer.IPing;
import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.PingUrl;

public class SimpleRibbonConfiguration {

	@Autowired
	public IClientConfig ribbonConfig;
	

	public IPing ping(IClientConfig clientConfig) {
		return new PingUrl();
	}

	public IRule rule(IClientConfig clientConfig) {
		return new AvailabilityFilteringRule();
	}

}
